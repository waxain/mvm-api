import { IMovie } from '../../src/modules/json-database/interfaces/db.interface';

export const sampleCreateMovie: IMovie = {
  title: 'Dancing with Wolves',
  genres: ['Western', 'War'],
  year: 1990,
  runtime: 181,
  director: 'Kevin Costner',
  actors: 'Kevin Costner, Mary McDonnell,Graham Greene',
  plot: 'Lt. John Dunbar is dubbed a hero after he accidentally leads Union troops to a victory during the Civil War. He requests a position on the western frontier, but finds it deserted. He soon finds out he is not alone, but meets a wolf he dubs "Two-socks" and a curious Indian tribe. Dunbar quickly makes friends with the tribe, and discovers a white woman who was raised by the Indians. He gradually earns the respect of these native people, and sheds his white-man\'s ways',
  posterUrl:
    'https://m.media-amazon.com/images/M/MV5BMTY3OTI5NDczN15BMl5BanBnXkFtZTcwNDA0NDY3Mw@@._V1_FMjpg_UX1000_.jpg',
};

export const sampleMovie: IMovie = {
  id: 5000,
  title: 'Dancing with Wolves',
  genres: ['Western', 'War'],
  year: 1990,
  runtime: 181,
  director: 'Kevin Costner',
  actors: 'Kevin Costner, Mary McDonnell,Graham Greene',
  plot: 'Lt. John Dunbar is dubbed a hero after he accidentally leads Union troops to a victory during the Civil War. He requests a position on the western frontier, but finds it deserted. He soon finds out he is not alone, but meets a wolf he dubs "Two-socks" and a curious Indian tribe. Dunbar quickly makes friends with the tribe, and discovers a white woman who was raised by the Indians. He gradually earns the respect of these native people, and sheds his white-man\'s ways',
  posterUrl:
    'https://m.media-amazon.com/images/M/MV5BMTY3OTI5NDczN15BMl5BanBnXkFtZTcwNDA0NDY3Mw@@._V1_FMjpg_UX1000_.jpg',
};
