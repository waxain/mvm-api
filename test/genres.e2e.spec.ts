import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { GenresService } from '../src/modules/movies/services/genres.service';

describe('Genres', () => {
  let app: INestApplication;
  const genresService = {
    list: () => {
      return { items: ['Western', 'Comedy'] };
    },
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [],
      imports: [AppModule],
    })
      .overrideProvider(GenresService)
      .useValue(genresService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET genres`, () => {
    return request(app.getHttpServer())
      .get('/movie-genres/list')
      .expect(200)
      .expect(genresService.list());
  });
  afterAll(async () => {
    await app.close();
  });
});
