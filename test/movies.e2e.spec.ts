import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { StatusBoolean } from '@shared/dto/status';
import { AppModule } from '../src/app.module';
import { sampleCreateMovie, sampleMovie } from './mocks/movie.mock';
import { MoviesService } from '../src/modules/movies/services/movies.service';

describe('Movies', () => {
  let app: INestApplication;
  const moviesService = {
    list: () => {
      return { items: [sampleMovie] };
    },
    create: () => {
      const response: StatusBoolean = { status: true };
      return response;
    },
  };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [],
      imports: [AppModule],
    })
      .overrideProvider(MoviesService)
      .useValue(moviesService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET movies`, () => {
    return request(app.getHttpServer())
      .get('/movie/list')
      .expect(200)
      .expect(moviesService.list());
  });

  it(`/GET movies`, () => {
    return request(app.getHttpServer())
      .get('/movie/list')
      .expect(200)
      .expect(moviesService.list());
  });

  it(`/POST movie OK`, () => {
    return request(app.getHttpServer())
      .post('/movie')
      .send(sampleCreateMovie)
      .expect(201)
      .expect({ status: true });
  });

  afterAll(async () => {
    await app.close();
  });
});
