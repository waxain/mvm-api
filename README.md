## Requirements

Redis installed.  
Tested on Node v14.17.6

## Installation

```bash
$ npm install  
copy .env.example to .env  
set proper redis connection in .env  

You may also use docker image (docker-composer.yml).
```

## Documentation

http://localhost:{APP_LISTENING_PORT}/swagger

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```
