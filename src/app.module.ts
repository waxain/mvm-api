import { Logger, Module, ModuleMetadata } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { BullModule } from '@nestjs/bull';
import { MoviesModule } from './modules/movies.module';
import { JsonDatabaseModule } from './modules/json-database/json-database.module';

export const appModuleMetadata: ModuleMetadata = {
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_HOST,
        port: +process.env.REDIS_PORT,
      },
    }),
    MoviesModule,
    JsonDatabaseModule,
  ],
  providers: [Logger],
};
@Module(appModuleMetadata)
export class AppModule {}
