export enum EEnv {
  DEVELOP = 'dev',
  PRODUCTION = 'prod',
  TEST = 'test',
}
