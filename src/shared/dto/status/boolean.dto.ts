import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsDefined, IsOptional, IsString } from 'class-validator';

export class StatusBoolean {
  @ApiProperty({
    example: false,
    description: 'Operation status',
    required: true,
  })
  @IsBoolean()
  @IsDefined()
  status: boolean;

  @ApiProperty({
    example: 'Duplicate entry',
    description: 'Additional information',
    required: false,
  })
  @IsOptional()
  @IsString()
  message?: string;
}
