export interface IFilterList {
  duration?: number;
  genres?: string[];
}
