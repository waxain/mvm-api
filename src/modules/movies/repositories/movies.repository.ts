import { Injectable } from '@nestjs/common';
import { IMovie } from 'modules/json-database/interfaces/db.interface';
import { JsonDatabaseRepository } from '../../json-database/repositories/json-database.repository';

@Injectable()
export class MoviesRepository extends JsonDatabaseRepository {
  protected path = 'movies';

  save(data: IMovie): void {
    data.id = this.nextId();
    super.save(data);
  }

  get(): IMovie[] {
    return super.get();
  }

  nextId(movies?: IMovie[]): number {
    if (!movies) {
      movies = this.get();
    }
    return Math.max(...movies.map((o) => o.id), 0) + 1;
  }

  random(movies?: IMovie[]): IMovie {
    if (!movies) {
      movies = this.get();
    }
    const rand: number = Math.floor(Math.random() * movies.length);
    return movies[rand];
  }

  filterByDuration(duration: number, movies?: IMovie[]): IMovie[] {
    if (!movies) {
      movies = this.get();
    }
    return movies.filter((movie: IMovie) => {
      return movie.runtime >= +duration - 10 && movie.runtime <= +duration + 10;
    });
  }

  filterByGenres(genres: string[], movies?: IMovie[]): IMovie[] {
    if (!movies) {
      movies = this.get();
    }

    const filtered: IMovie[] = [];

    movies.filter((movie: IMovie) => {
      const matchGenres: number = movie.genres.filter(
        Set.prototype.has,
        new Set(genres),
      ).length;

      if (matchGenres > 0) {
        filtered.push({
          id: movie.id,
          title: movie.title,
          genres: movie.genres,
          year: movie.year,
          runtime: movie.runtime,
          director: movie.director,
          actors: movie.actors,
          plot: movie.plot,
          posterUrl: movie.posterUrl,
          matchGenres: matchGenres,
        });
        return true;
      }
      return false;
    });

    return filtered.sort((a: IMovie, b: IMovie) =>
      a.matchGenres > b.matchGenres ? -1 : 1,
    );
  }
}
