import { Injectable } from '@nestjs/common';
import { JsonDatabaseRepository } from '../../json-database/repositories/json-database.repository';

@Injectable()
export class GenresRepository extends JsonDatabaseRepository {
  protected path = 'genres';

  get(): string[] {
    return super.get();
  }
}
