import { MoviesRepository } from '../repositories/movies.repository';
import { Test } from '@nestjs/testing';
import { JsonDatabaseModule } from '../../json-database/json-database.module';
import { sampleCreateMovie } from '../../../../test/mocks/movie.mock';
import * as fs from 'fs';
import 'jest-extended';
import { IMovie } from '../../json-database/interfaces/db.interface';

describe('MoviesRepository', () => {
  let moviesRepository: MoviesRepository;

  const jsonData = fs.readFileSync(process.env.MOVIES_DB_PATH, 'utf8');
  const db = JSON.parse(jsonData);

  beforeEach(async () => {
    jest.resetAllMocks();

    const moduleRef = await Test.createTestingModule({
      imports: [JsonDatabaseModule],
      providers: [MoviesRepository],
    }).compile();

    moviesRepository = moduleRef.get<MoviesRepository>(MoviesRepository);
  });

  describe('nextId', () => {
    it('should return next primary id', async () => {
      expect(moviesRepository.nextId()).toBe(db.movies.slice(-1)[0].id + 1);
    });
  });

  describe('save', () => {
    it('should save movie', async () => {
      const writeFileSpy = jest.spyOn(fs, 'writeFile').mockReturnValue(void 0);

      expect(moviesRepository.save(sampleCreateMovie));

      expect(writeFileSpy).toHaveBeenCalledTimes(1);

      writeFileSpy.mockClear();
    });
  });

  describe('get', () => {
    it('should get movies', async () => {
      await expect(moviesRepository.get()).toEqual(db.movies);
    });
  });

  describe('random', () => {
    it('should get random movie', async () => {
      expect(moviesRepository.random()).toBeOneOf(db.movies);
    });
  });

  describe('filterByDuration', () => {
    it('should filter movies by duration 170 (runtime between <duration - 10> and <duration + 10>)', async () => {
      const result = db.movies.filter((movie: IMovie) => {
        return [
          19, 25, 52, 72, 73, 82, 98, 101, 113, 116, 128, 129, 132, 133,
        ].includes(movie.id);
      });

      expect(moviesRepository.filterByDuration(170)).toEqual(result);
    });
  });

  describe('filterByDuration0', () => {
    it('should filter movies by duration 0', async () => {
      expect(moviesRepository.filterByDuration(0)).toEqual([]);
    });
  });

  describe('filterByDuration-1', () => {
    it('should filter movies by duration -111', async () => {
      expect(moviesRepository.filterByDuration(-111)).toEqual([]);
    });
  });

  describe('filterByGenres', () => {
    it('should filter movies genres', async () => {
      const result: IMovie[] = moviesRepository.filterByGenres([
        'Drama',
        'Fantasy',
        'War',
      ]);

      const equalResult1: IMovie = db.movies.find(
        (movie: IMovie) => movie.id === 34,
      );
      const equalResult2: IMovie = db.movies.find(
        (movie: IMovie) => movie.id === 23,
      );
      const equalResult3: IMovie = db.movies.find(
        (movie: IMovie) => movie.id === 66,
      );

      expect(result[0].id).toEqual(equalResult1.id);
      expect(result[1].id).toEqual(equalResult2.id);
      expect(result[2].id).toEqual(equalResult3.id);
    });
  });
});
