import { Test } from '@nestjs/testing';
import { JsonDatabaseModule } from '../../json-database/json-database.module';
import * as fs from 'fs';
import 'jest-extended';
import { GenresRepository } from './genres.repository';

describe('GenresRepository', () => {
  let genresRepository: GenresRepository;

  const jsonData = fs.readFileSync(process.env.MOVIES_DB_PATH, 'utf8');
  const db = JSON.parse(jsonData);

  beforeEach(async () => {
    jest.resetAllMocks();

    const moduleRef = await Test.createTestingModule({
      imports: [JsonDatabaseModule],
      providers: [GenresRepository],
    }).compile();

    genresRepository = moduleRef.get<GenresRepository>(GenresRepository);
  });

  describe('get', () => {
    it('should get genres', async () => {
      await expect(genresRepository.get()).toEqual(db.genres);
    });
  });
});
