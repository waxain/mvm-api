import { Body, Controller, Get, Post, Query, Request } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';

import { StatusBoolean } from '@shared/dto/status';
import { MoviesService } from '../services/movies.service';
import { MovieCreateDto } from '../dto/movie-create.dto';
import { MovieListFilterDto, MovieListItemsDto } from '../dto/movie-list.dto';

@Controller('movie')
@ApiTags('Movie')
@ApiBadRequestResponse()
@ApiInternalServerErrorResponse()
export class MoviesController {
  constructor(private readonly moviesService: MoviesService) {}

  @Post('')
  @ApiOperation({
    operationId: 'createMovie',
    summary: 'Create movie.',
  })
  @ApiOkResponse({ type: StatusBoolean })
  async create(
    @Request() request,
    @Body() body: MovieCreateDto,
  ): Promise<StatusBoolean> {
    return await this.moviesService.create(body);
  }

  @Get('list')
  @ApiOperation({
    operationId: 'getMovies',
    summary: 'Get movies list.',
  })
  @ApiOkResponse({ type: MovieListItemsDto })
  async list(@Query() query: MovieListFilterDto): Promise<MovieListItemsDto> {
    return await this.moviesService.list(query);
  }
}
