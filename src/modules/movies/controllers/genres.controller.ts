import { Controller, Get } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { GenresService } from '../services/genres.service';
import { GenreListItemsDto } from '../dto/genre-list.dto';

@Controller('movie-genres')
@ApiTags('Movie genres')
@ApiBadRequestResponse()
@ApiInternalServerErrorResponse()
export class GenresController {
  constructor(private readonly genresService: GenresService) {}

  @Get('list')
  @ApiOperation({
    operationId: 'getMovieGenres',
    summary: 'Get movie genres list.',
  })
  @ApiOkResponse({ type: GenreListItemsDto })
  async list(): Promise<GenreListItemsDto> {
    return await this.genresService.list();
  }
}
