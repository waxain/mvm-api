import { GenresController } from './genres.controller';
import { GenresService } from '../services/genres.service';
import { GenresRepository } from '../repositories/genres.repository';
import { Test } from '@nestjs/testing';
import { JsonDatabaseModule } from '../../json-database/json-database.module';

describe('GenresController', () => {
  let genresController: GenresController;
  let genresService: GenresService;

  beforeEach(async () => {
    jest.resetAllMocks();

    const moduleRef = await Test.createTestingModule({
      controllers: [GenresController],
      imports: [JsonDatabaseModule],
      providers: [GenresService, GenresRepository],
    }).compile();

    genresService = await moduleRef.get<GenresService>(GenresService);
    genresController = await moduleRef.get<GenresController>(GenresController);
  });

  describe('list', () => {
    it('should be called list() and return genres', async () => {
      const result = { items: ['War', 'Comedy'] };
      const spy = jest
        .spyOn(genresService, 'list')
        .mockImplementation(() => Promise.resolve(result));

      expect(await genresController.list()).toBe(result);
      expect(spy).toBeCalledTimes(1);
    });
  });
});
