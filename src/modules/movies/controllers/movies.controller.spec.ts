import { MoviesService } from '../services/movies.service';
import { MoviesRepository } from '../repositories/movies.repository';
import { MoviesController } from './movies.controller';
import { MovieListFilterDto, MovieListItemsDto } from '../dto/movie-list.dto';
import { MovieDto } from '../dto/movie.dto';
import { sampleMovie } from '../../../../test/mocks/movie.mock';
import { Test } from '@nestjs/testing';
import { JsonDatabaseModule } from '../../json-database/json-database.module';
import { BullModule, getQueueToken } from '@nestjs/bull';
import { QueueConstant } from '../constants/queue.constant';
import { StatusBoolean } from '@shared/dto/status';

describe('MoviesController', () => {
  let moviesController: MoviesController;
  let moviesService: MoviesService;
  let mockQueue;

  beforeEach(async () => {
    jest.resetAllMocks();

    mockQueue = {
      add: jest.fn(),
    };

    const moduleRef = await Test.createTestingModule({
      controllers: [MoviesController],
      imports: [
        JsonDatabaseModule,
        BullModule.registerQueue({
          name: QueueConstant.MOVIE_SAVE,
          redis: {
            host: '0.0.0.0',
            port: 6380,
          },
          processors: [jest.fn()],
        }),
      ],
      providers: [MoviesService, MoviesRepository],
    })
      .overrideProvider(getQueueToken(QueueConstant.MOVIE_SAVE))
      .useValue(mockQueue)
      .compile();

    moviesService = moduleRef.get<MoviesService>(MoviesService);
    moviesController = moduleRef.get<MoviesController>(MoviesController);
  });

  describe('list', () => {
    it('should call list() and return an random movie', async () => {
      const movie: MovieDto = sampleMovie;

      const result: MovieListItemsDto = {
        items: [movie],
      };

      const query: MovieListFilterDto = new MovieListFilterDto();

      const spy = jest
        .spyOn(moviesService, 'list')
        .mockImplementation(() => Promise.resolve(result));

      expect(await moviesController.list(query)).toBe(result);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('filteredListByRuntime', () => {
    it('should call list() and return list of filtered movies by runtine', async () => {
      const movie: MovieDto = sampleMovie;
      const result: MovieListItemsDto = {
        items: [movie, movie],
      };

      const query: MovieListFilterDto = new MovieListFilterDto();
      query.duration = 180;

      const spy = jest
        .spyOn(moviesService, 'list')
        .mockImplementation(() => Promise.resolve(result));

      expect(await moviesController.list(query)).toBe(result);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('filteredListByGenres', () => {
    it('should call list() and return list of filtered movies by genres', async () => {
      const movie: MovieDto = sampleMovie;
      const result: MovieListItemsDto = {
        items: [movie],
      };

      const query: MovieListFilterDto = new MovieListFilterDto();
      query.genres = ['Western', 'War'];

      const spy = jest
        .spyOn(moviesService, 'list')
        .mockImplementation(() => Promise.resolve(result));

      expect(await moviesController.list(query)).toBe(result);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('filteredListByRuntimeAndGenres', () => {
    it('should call list() and return list of filtered movies by runtime and genres', async () => {
      const movie: MovieDto = sampleMovie;
      const result: MovieListItemsDto = {
        items: [movie],
      };

      const query: MovieListFilterDto = new MovieListFilterDto();
      query.genres = ['Western', 'War'];
      query.duration = 180;

      const spy = jest
        .spyOn(moviesService, 'list')
        .mockImplementation(() => Promise.resolve(result));

      expect(await moviesController.list(query)).toBe(result);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('create', () => {
    it('should call create and status true', async () => {
      const movie: MovieDto = sampleMovie;
      const result: StatusBoolean = new StatusBoolean();

      const spy = jest
        .spyOn(moviesService, 'create')
        .mockImplementation(() => Promise.resolve(result));

      const req = { body: null, params: null };
      req.body = jest.fn().mockReturnValue(req);
      req.params = jest.fn().mockReturnValue(req);

      expect(await moviesController.create(req, movie)).toBe(result);
      expect(spy).toBeCalledTimes(1);
    });
  });
});
