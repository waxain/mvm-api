import { Process, Processor } from '@nestjs/bull';
import { QueueConstant } from '../constants/queue.constant';
import { Job } from 'bull';
import { Injectable } from '@nestjs/common';
import { MoviesService } from '../services/movies.service';

@Processor(QueueConstant.MOVIE_SAVE)
@Injectable()
export class SaveMovieConsumer {
  constructor(private readonly moviesService: MoviesService) {}

  @Process()
  async saveMovie(job: Job): Promise<void> {
    await this.moviesService.saveMovie(job.data.movie);
  }
}
