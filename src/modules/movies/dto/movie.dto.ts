import {
  ArrayNotEmpty,
  IsArray,
  IsDefined,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Length,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { IMovie } from '../../json-database/interfaces/db.interface';
import { GenresExists } from '../validators/genres-exist.decorator';

export class BaseMovieDto implements IMovie {
  @ApiProperty({
    example: 'Dancing with Wolves',
    required: true,
  })
  @IsDefined()
  @Length(1, 255)
  title: string;

  @ApiProperty({
    isArray: true,
    example: ['Western', 'War'],
    required: true,
  })
  @IsDefined()
  @IsArray()
  @ArrayNotEmpty({ always: true })
  @IsNotEmpty({ each: true })
  @GenresExists()
  genres: string[];

  @ApiProperty({
    example: 1990,
    required: true,
  })
  @IsDefined()
  @IsNumber()
  year: number;

  @ApiProperty({
    example: 181,
    required: true,
  })
  @IsNumber()
  runtime: number;

  @ApiProperty({
    example: 'Kevin Costner',
    required: true,
  })
  @IsDefined()
  @IsString()
  @Length(1, 255)
  director: string;

  @ApiProperty({
    example: 'Kevin Costner, Mary McDonnell,Graham Greene',
    required: false,
  })
  @IsOptional()
  @IsString()
  actors?: string;

  @ApiProperty({
    example:
      'Lt. John Dunbar is dubbed a hero after he accidentally leads Union troops to a victory during the Civil War. He requests a position on the western frontier, but finds it deserted. He soon finds out he is not alone, but meets a wolf he dubs "Two-socks" and a curious Indian tribe. Dunbar quickly makes friends with the tribe, and discovers a white woman who was raised by the Indians. He gradually earns the respect of these native people, and sheds his white-man\'s ways',
    required: false,
  })
  @IsOptional()
  @IsString()
  plot?: string;

  @ApiProperty({
    example:
      'https://m.media-amazon.com/images/M/MV5BMTY3OTI5NDczN15BMl5BanBnXkFtZTcwNDA0NDY3Mw@@._V1_FMjpg_UX1000_.jpg',
    required: false,
  })
  @IsOptional()
  @IsString()
  posterUrl?: string;
}

export class MovieDto extends BaseMovieDto implements IMovie {
  @ApiProperty({
    example: 1,
    required: false,
  })
  @IsOptional()
  @IsNumber()
  id?: number;
}
