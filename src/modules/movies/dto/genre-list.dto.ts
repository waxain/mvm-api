import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsDefined } from 'class-validator';

export class GenreListItemsDto {
  @ApiProperty({
    isArray: true,
    type: String,
    required: true,
  })
  @IsArray()
  @IsDefined()
  items: string[];
}
