import {
  ArrayUnique,
  IsArray,
  IsDefined,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  isString,
  Validate,
  ValidateNested,
} from 'class-validator';
import { MovieDto } from './movie.dto';
import { Transform, Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { IFilterList } from '../interfaces/filter-list.interface';
import { GenresExistsRule } from '../validators/genres-exists-rule.validator';

export class MovieListItemsDto {
  @ApiProperty({
    isArray: true,
    type: MovieDto,
    required: true,
  })
  @Type(() => MovieDto)
  @ValidateNested()
  @IsDefined()
  items: MovieDto[];
}

export class MovieListFilterDto implements IFilterList {
  @ApiProperty({
    required: false,
    type: Number,
    description: 'Duration of movie (in minutes)',
  })
  @IsOptional()
  @IsNumberString()
  duration?: number = null;

  @ApiProperty({
    type: [String],
    uniqueItems: true,
    isArray: true,
    example: ['Comedy', 'Fantasy', 'Crime'],
    description: 'Array of genres',
    name: 'genres',
    required: false,
  })
  @Transform(({ value }) => (isString(value) ? [value] : value))
  @IsArray()
  @IsOptional()
  @ArrayUnique()
  @IsNotEmpty({ each: true })
  @Validate(GenresExistsRule)
  genres?: string[] = null;
}
