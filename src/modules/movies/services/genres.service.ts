import { Injectable } from '@nestjs/common';
import { GenresRepository } from '../repositories/genres.repository';
import { GenreListItemsDto } from '../dto/genre-list.dto';

@Injectable()
export class GenresService {
  constructor(private genresRepository: GenresRepository) {}

  async list(): Promise<GenreListItemsDto> {
    const response: GenreListItemsDto = new GenreListItemsDto();
    response.items = this.genresRepository.get().sort();
    return response;
  }
}
