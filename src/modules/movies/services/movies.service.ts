import { Injectable } from '@nestjs/common';
import { MovieCreateDto } from '../dto/movie-create.dto';
import { MovieListFilterDto, MovieListItemsDto } from '../dto/movie-list.dto';
import { StatusBoolean } from '@shared/dto/status';
import { MoviesRepository } from '../repositories/movies.repository';
import { IMovie } from '../../json-database/interfaces/db.interface';
import { InjectQueue } from '@nestjs/bull';
import { QueueConstant } from '../constants/queue.constant';
import { Queue } from 'bull';

@Injectable()
export class MoviesService {
  constructor(
    @InjectQueue(QueueConstant.MOVIE_SAVE)
    private saveMovieQueue: Queue,
    private moviesRepository: MoviesRepository,
  ) {}

  async create(body: MovieCreateDto): Promise<StatusBoolean> {
    try {
      const data: IMovie = {
        title: body.title,
        year: body.year,
        runtime: body.runtime,
        genres: body.genres,
        director: body.director,
        actors: body.actors ?? '',
        plot: body.plot,
        posterUrl: body.posterUrl ?? '',
      };

      await this.saveMovieQueue.add({
        movie: data,
      });
    } catch (e) {
      throw e;
    }
    return { status: true };
  }

  async saveMovie(movie: IMovie): Promise<void> {
    await this.moviesRepository.save(movie);
  }

  async list(query: MovieListFilterDto): Promise<MovieListItemsDto> {
    const response: MovieListItemsDto = new MovieListItemsDto();
    let movies: IMovie[] = this.moviesRepository.get();

    if (movies.length > 0) {
      if (!query.genres && !query.duration) {
        response.items = [this.moviesRepository.random()];
      } else {
        if (query.genres) {
          movies = this.moviesRepository.filterByGenres(query.genres);
        }

        if (query.duration) {
          movies = this.moviesRepository.filterByDuration(
            query.duration,
            movies,
          );
        }

        response.items = movies;
      }
    }

    return response;
  }
}
