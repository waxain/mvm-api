import { Test } from '@nestjs/testing';
import { JsonDatabaseModule } from '../../json-database/json-database.module';
import { GenresService } from './genres.service';
import { GenresRepository } from '../repositories/genres.repository';
import { GenreListItemsDto } from '../dto/genre-list.dto';

describe('GenresService', () => {
  let genresService: GenresService;
  let genresRepository: GenresRepository;

  beforeEach(async () => {
    jest.resetAllMocks();

    const moduleRef = await Test.createTestingModule({
      imports: [JsonDatabaseModule],
      providers: [GenresService, GenresRepository],
    }).compile();

    genresService = moduleRef.get<GenresService>(GenresService);
    genresRepository = moduleRef.get<GenresRepository>(GenresRepository);
  });

  describe('list', () => {
    it('should call get()', async () => {
      const items: string[] = ['Western', 'War'];
      const result: GenreListItemsDto = {
        items: items,
      };

      const spy = jest
        .spyOn(genresRepository, 'get')
        .mockImplementation(() => items);

      expect(await genresService.list()).toEqual(result);
      expect(spy).toBeCalledTimes(1);
    });
  });
});
