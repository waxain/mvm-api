import { MoviesService } from './movies.service';
import { MoviesRepository } from '../repositories/movies.repository';
import { MovieListFilterDto, MovieListItemsDto } from '../dto/movie-list.dto';
import { MovieDto } from '../dto/movie.dto';
import { BullModule, getQueueToken } from '@nestjs/bull';
import { QueueConstant } from '../constants/queue.constant';
import { Test } from '@nestjs/testing';
import {
  sampleCreateMovie,
  sampleMovie,
} from '../../../../test/mocks/movie.mock';
import { JsonDatabaseModule } from '../../json-database/json-database.module';

describe('MoviesService', () => {
  let moviesService: MoviesService;
  let moviesRepository: MoviesRepository;
  let mockQueue;

  beforeEach(async () => {
    jest.resetAllMocks();

    mockQueue = {
      add: jest.fn(),
    };
    const moduleRef = await Test.createTestingModule({
      imports: [
        JsonDatabaseModule,
        BullModule.registerQueue({
          name: QueueConstant.MOVIE_SAVE,
          redis: {
            host: '0.0.0.0',
            port: 6380,
          },
          processors: [jest.fn()],
        }),
      ],
      providers: [MoviesService, MoviesRepository],
    })
      .overrideProvider(getQueueToken(QueueConstant.MOVIE_SAVE))
      .useValue(mockQueue)
      .compile();

    moviesService = moduleRef.get<MoviesService>(MoviesService);
    moviesRepository = moduleRef.get<MoviesRepository>(MoviesRepository);
  });

  describe('list', () => {
    it('should call random()', async () => {
      const movie: MovieDto = sampleMovie;

      const result: MovieListItemsDto = {
        items: [movie],
      };

      const query: MovieListFilterDto = new MovieListFilterDto();

      const spy = jest
        .spyOn(moviesRepository, 'random')
        .mockImplementation(() => movie);

      expect(await moviesService.list(query)).toEqual(result);
      expect(spy).toBeCalledTimes(1);
    });
  });

  describe('list-with-duration-filter', () => {
    it('should call filterByDuration()', async () => {
      const movie: MovieDto = sampleMovie;

      const result: MovieListItemsDto = {
        items: [movie],
      };

      const query: MovieListFilterDto = new MovieListFilterDto();
      query.duration = 180;

      const spy1 = jest
        .spyOn(moviesRepository, 'get')
        .mockImplementation(() => [movie]);

      const spy2 = jest
        .spyOn(moviesRepository, 'filterByDuration')
        .mockImplementation(() => [movie]);

      expect(await moviesService.list(query)).toEqual(result);
      expect(spy1).toBeCalledTimes(1);
      expect(spy2).toBeCalledTimes(1);
    });
  });

  describe('list-with-genres-filter', () => {
    it('should call filterByGenres()', async () => {
      const movie: MovieDto = sampleMovie;

      const result: MovieListItemsDto = {
        items: [movie],
      };

      const query: MovieListFilterDto = new MovieListFilterDto();
      query.genres = ['Western'];

      const spy1 = jest
        .spyOn(moviesRepository, 'get')
        .mockImplementation(() => [movie]);

      const spy2 = jest
        .spyOn(moviesRepository, 'filterByGenres')
        .mockImplementation(() => [movie]);

      expect(await moviesService.list(query)).toEqual(result);
      expect(spy1).toBeCalledTimes(1);
      expect(spy2).toBeCalledTimes(1);
    });
  });

  describe('list-with-genres-and-duration-filter', () => {
    it('should call filterByGenres() and filterByDuration()', async () => {
      const movie: MovieDto = sampleMovie;

      const result: MovieListItemsDto = {
        items: [movie],
      };

      const query: MovieListFilterDto = new MovieListFilterDto();
      query.duration = 180;
      query.genres = ['Western'];

      const spy1 = jest
        .spyOn(moviesRepository, 'get')
        .mockImplementation(() => [movie]);

      const spy2 = jest
        .spyOn(moviesRepository, 'filterByDuration')
        .mockImplementation(() => [movie]);

      const spy3 = jest
        .spyOn(moviesRepository, 'filterByGenres')
        .mockImplementation(() => [movie]);

      expect(await moviesService.list(query)).toEqual(result);
      expect(spy1).toBeCalledTimes(1);
      expect(spy2).toBeCalledTimes(1);
      expect(spy3).toBeCalledTimes(1);
    });
  });

  describe('create', () => {
    it('should call create movie queue once', async () => {
      const movie: MovieDto = sampleCreateMovie;
      await moviesService.create(movie);

      expect(mockQueue.add).toBeCalledTimes(1);
    });
  });

  describe('create-with-error', () => {
    it('should throw an error with queue', async () => {
      jest.spyOn(mockQueue, 'add').mockImplementation(() => {
        throw new Error('Problem with queue');
      });

      const movie: MovieDto = sampleCreateMovie;
      await expect(async () => {
        await moviesService.create(movie);
      }).rejects.toThrow('Problem with queue');
    });
  });

  describe('saveMovie', () => {
    it('should save new movie in db', async () => {
      const movie: MovieDto = sampleCreateMovie;
      jest.spyOn(moviesRepository, 'save').mockImplementation(() => {
        void 0;
      });
      await expect(moviesService.saveMovie(movie)).resolves.not.toThrow();
    });
  });
});
