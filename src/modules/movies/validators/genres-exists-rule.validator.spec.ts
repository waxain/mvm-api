import { GenresExistsRule } from './genres-exists-rule.validator';
import { GenresRepository } from '../repositories/genres.repository';

describe('GenresExistsRule', () => {
  let genresExistsRule: GenresExistsRule;

  beforeEach(async () => {
    genresExistsRule = new GenresExistsRule(
      new GenresRepository({
        genres: ['Western', 'Comedy', 'Action'],
        movies: [],
      }),
    );
  });

  describe('validate', () => {
    it('should be true', async () => {
      expect(genresExistsRule.validate(['Western'])).toBe(true);
    });

    it('should be true', async () => {
      expect(genresExistsRule.validate(['Western', 'Comedy'])).toBe(true);
    });

    it('should be false', async () => {
      expect(genresExistsRule.validate('')).toBe(false);
    });

    it('should be false', async () => {
      expect(genresExistsRule.validate(['Western', 'Drama'])).toBe(false);
    });
  });
});
