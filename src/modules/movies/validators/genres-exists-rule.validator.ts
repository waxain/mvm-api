import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  isArray,
} from 'class-validator';
import { GenresRepository } from '../repositories/genres.repository';
import { Inject, Injectable } from '@nestjs/common';

@ValidatorConstraint({ name: 'GenresExists', async: true })
@Injectable()
export class GenresExistsRule implements ValidatorConstraintInterface {
  constructor(
    @Inject('GenresRepository')
    private readonly genresRepository: GenresRepository,
  ) {}

  validate(genres: any) {
    const genresDb: string[] = this.genresRepository.get();

    if (!isArray(genres)) {
      genres = [genres];
    }

    const filteredGenres: string[] = genres.filter((genre: string) => {
      return genresDb.includes(genre);
    });

    return filteredGenres.length === genres.length;
  }

  defaultMessage(args: ValidationArguments) {
    return `One of given movie genres (${args.value}) does not exist!`;
  }
}
