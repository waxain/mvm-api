import { registerDecorator, ValidationOptions } from 'class-validator';
import { GenresExistsRule } from './genres-exists-rule.validator';

export function GenresExists(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'GenresExists',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: GenresExistsRule,
    });
  };
}
