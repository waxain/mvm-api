import { Module, ModuleMetadata } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MoviesController } from './movies/controllers/movies.controller';
import { MoviesService } from './movies/services/movies.service';
import { MoviesRepository } from './movies/repositories/movies.repository';
import { JsonDatabaseModule } from './json-database/json-database.module';
import { GenresRepository } from './movies/repositories/genres.repository';
import { GenresExistsRule } from './movies/validators/genres-exists-rule.validator';
import { BullModule } from '@nestjs/bull';
import { QueueConstant } from './movies/constants/queue.constant';
import { SaveMovieConsumer } from './movies/consumer/save-movie.consumer';
import { GenresController } from './movies/controllers/genres.controller';
import { GenresService } from './movies/services/genres.service';

export const moviesModuleMetadata: ModuleMetadata = {
  controllers: [MoviesController, GenresController],
  providers: [
    GenresExistsRule,
    MoviesService,
    GenresService,
    SaveMovieConsumer,
    MoviesRepository,
    GenresRepository,
  ],
  exports: [MoviesService],
  imports: [
    ConfigModule.forRoot(),
    JsonDatabaseModule,
    BullModule.forRoot({
      redis: {
        host: process.env.REDIS_HOST,
        port: +process.env.REDIS_PORT,
      },
    }),
    BullModule.registerQueue({
      name: QueueConstant.MOVIE_SAVE,
    }),
  ],
};

@Module(moviesModuleMetadata)
export class MoviesModule {}
