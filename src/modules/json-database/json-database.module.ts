import { Module } from '@nestjs/common';
import { jsonDatabaseProvider } from './providers/json-database.provider';

@Module({
  providers: [...jsonDatabaseProvider],
  exports: [...jsonDatabaseProvider],
})
export class JsonDatabaseModule {}
