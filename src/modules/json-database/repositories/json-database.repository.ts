import { Inject } from '@nestjs/common';
import * as fs from 'fs';
import { WrongDbFileStructureException } from '../exceptions/wrong-db-file-structure.exception';
import { IDb } from '../interfaces/db.interface';
import { CantWriteDbFileException } from '../exceptions/cant-write-db-file.exception';

export abstract class JsonDatabaseRepository {
  protected db: IDb;

  protected path = '';

  constructor(@Inject('JSON_DATABASE_CONNECTION') db: IDb) {
    this.db = db;
  }

  get(): any {
    if (this.db.hasOwnProperty(this.path)) {
      return this.db[this.path];
    }
    throw new WrongDbFileStructureException();
  }

  save(data: any): void {
    const currentData = this.db;

    if (currentData.hasOwnProperty(this.path)) {
      currentData[this.path] = currentData[this.path].concat(data);

      fs.writeFile(
        process.env.MOVIES_DB_PATH,
        JSON.stringify(currentData, null, 4),
        'utf8',
        (e) => {
          if (e) {
            throw new CantWriteDbFileException();
          }
        },
      );
    } else {
      throw new WrongDbFileStructureException();
    }
  }
}
