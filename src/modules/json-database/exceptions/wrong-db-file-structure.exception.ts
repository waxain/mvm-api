export class WrongDbFileStructureException extends Error {
  constructor() {
    super('Wrong movies db file structure.');
  }
}
