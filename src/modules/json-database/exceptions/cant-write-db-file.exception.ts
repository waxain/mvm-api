export class CantWriteDbFileException extends Error {
  constructor() {
    super("Can't write db file!");
  }
}
