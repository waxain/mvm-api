export class DbFileNotExistsException extends Error {
  constructor() {
    super('Movies db file not exists.');
  }
}
