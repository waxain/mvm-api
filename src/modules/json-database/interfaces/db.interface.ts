export interface IMovie {
  id?: number;
  title: string;
  genres: string[];
  year: number;
  runtime: number;
  director: string;
  actors?: string;
  plot?: string;
  posterUrl?: string;
  matchGenres?: number;
}

export interface IDb {
  movies: IMovie[];
  genres: string[];
}
