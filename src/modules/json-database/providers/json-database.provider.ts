import * as fs from 'fs';
import { IDb } from '../interfaces/db.interface';
import * as path from 'path';

export const jsonDatabaseProvider = [
  {
    provide: 'JSON_DATABASE_CONNECTION',
    useFactory: async (): Promise<IDb> => {
      const dbFile: string = process.env.MOVIES_DB_PATH;

      if (!fs.existsSync(dbFile)) {
        const dbDir: string = path.dirname(dbFile);
        if (!fs.existsSync(dbDir)) {
          fs.mkdirSync(dbDir);
        }
        fs.copyFileSync('.db.example.json', dbFile);
      }

      const data = fs.readFileSync(dbFile, 'utf8');
      return JSON.parse(data);
    },
  },
];
