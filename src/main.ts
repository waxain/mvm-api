import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { Logger, ValidationError, ValidationPipe } from '@nestjs/common';
import * as helmet from 'helmet';
import {
  SwaggerModule,
  DocumentBuilder,
  SwaggerCustomOptions,
} from '@nestjs/swagger';
import { customExceptionFactory } from '@shared/utils/exceptions/validation.utils';
import { TrimPipe } from '@shared/transforms/trim.pipe';
import { isNullOrUndefined } from '@shared/utils/helpers/core.utils';
import { useContainer } from 'class-validator';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';
import { transports } from 'winston';

async function bootstrap() {
  if (isNullOrUndefined(process.env.NODE_ENV)) {
    throw new Error('Please set NODE_ENV in your .env file');
  } else {
    Logger.log(`[NODE_ENV] ${process.env.NODE_ENV}`);
  }

  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: WinstonModule.createLogger({
      level: process.env.LOGGER_LEVEL || 'debug',
      transports: [
        new transports.Console(),
        new transports.File({ filename: process.env.LOGGER_FILE_PATH }),
      ],
      format: winston.format.combine(
        winston.format.errors({ stack: true }),
        winston.format.timestamp(),
        winston.format.colorize({ all: true }),
        winston.format.printf(({ level, message, timestamp, stack }) => {
          if (stack) {
            return `${timestamp} ${level}: ${message} - ${stack}`;
          }
          return `${timestamp} ${level}: ${message}`;
        }),
      ),
    }),
  });

  app.useGlobalPipes(new TrimPipe());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      exceptionFactory: (errors: ValidationError[]) =>
        customExceptionFactory(errors),
    }),
  );
  app.enableCors();
  app.use(helmet());
  app.setGlobalPrefix('api');

  if (+process.env.OPEN_API_AVAILABLE) {
    const config = new DocumentBuilder()
      .setTitle('MvM')
      .setDescription('My Favorite Movies API')
      .setVersion('1.0')
      .build();
    const document = SwaggerModule.createDocument(app, config);
    const customOptions: SwaggerCustomOptions = {
      swaggerOptions: {
        tagsSorter: 'alpha',
        operationsSorter: 'alpha',
      },
      customSiteTitle: 'API docs',
    };
    SwaggerModule.setup('swagger', app, document, customOptions);
  }
  await app.listen(process.env.APP_LISTENING_PORT);
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
}
bootstrap();
