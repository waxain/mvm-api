// eslint-disable-next-line no-undef
const { pathsToModuleNameMapper } = require('ts-jest/utils');
// In the following statement, replace `./tsconfig` with the path to your `tsconfig` file
// which contains the path mapping (ie the `compilerOptions.paths` option):
// eslint-disable-next-line no-undef
const { compilerOptions } = require('./tsconfig');
const path = require('path');
const findFileUp = require('find-file-up');
// unfortunately, __dirname doesn't work to resolve the directory of
// of this file. So we need to find it. Since there is only one yarn.lock file in the system,
// let's use it.
const rootDir = path.parse(findFileUp.sync('package-lock.json')).dir;

let collectCoverageFrom = [];
collectCoverageFrom.push('src/*.(t|j)s');
collectCoverageFrom.push('src/**/*.(t|j)s');

// eslint-disable-next-line no-undef
module.exports = {
  setupFilesAfterEnv: ['jest-extended'],
  testEnvironment: 'node',
  cacheDirectory: `${__dirname}/.jest/cache`,
  collectCoverage: false,
  coverageDirectory: `${__dirname}/.jest/coverage`,
  collectCoverageFrom: collectCoverageFrom,
  coveragePathIgnorePatterns: [`${rootDir}/data/`, `${rootDir}/log/`, `${rootDir}/dist/`],
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'js', 'json', 'node'],
  // eslint-disable-next-line no-undef
  moduleNameMapper: {
    ...pathsToModuleNameMapper(compilerOptions.paths, {
      prefix: __dirname + '/src/',
    }),
  },
  testPathIgnorePatterns: ['/dist/'],
  setupFiles: ['<rootDir>/jest.set-env-vars.js'],
};
